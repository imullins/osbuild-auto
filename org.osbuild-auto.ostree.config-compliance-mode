#!/usr/bin/python3
"""
Configure the ostree-compliance-mode parameters

Configures the ostree-compliance-mode parameters in /etc/ostree-compliance-mode.conf
"""

import os
import sys

import osbuild.api
from osbuild.util import ostree

SCHEMA_2 = """
"options": {
  "additionalProperties": false,
  "required": ["deployment"],
  "properties": {
    "deployment": {
    "additionalProperties": false,
    "required": ["osname", "ref"],
    "properties": {
      "osname": {
        "description": "Name of the stateroot to be used in the deployment",
        "type": "string"
      },
      "ref": {
        "description": "OStree ref to create and use for deployment",
        "type": "string"
      },
      "serial": {
        "description": "The deployment serial (usually '0')",
        "type": "number",
        "default": 0
      }
    }  
  }
}
},
"inputs": {
  "type": "object",
  "additionalProperties": false,
  "required": ["commits"],
  "properties": {
    "commits": {
      "type": "object",
      "additionalProperties": true
    }
  }
}
"""


def parse_input(inputs):
    commits = inputs["commits"]
    data = commits["data"]
    refs = data["refs"]

    '''
    Dictionaries are insertion ordered from Python 3.7 onwards
    returning the ref at dict size-1 should always provide most recent commit id
    '''
    commit_list = refs.keys()
    latest_commit = list(commit_list)[-1]

    assert latest_commit, "Need at least one commit"
    return latest_commit


def build_path(tree, options):
    dep = options["deployment"]
    osname = dep["osname"]
    ref = dep["ref"]
    serial = dep.get("serial", 0)

    root = ostree.deployment_path(tree, osname, ref, serial)
    base = os.path.join(root, "etc")
    return base


def main(tree, inputs, options):
    base = build_path(tree, options)
    latest_commit = parse_input(inputs)

    if (latest_commit):
        try:
            with open(f"{base}/ostree-compliance-mode.conf", "w+") as f:
                f.write(latest_commit)
        except Exception as e:
            print(f"An error occurred while writing to the file: {e}")
            return 1

    return 0


if __name__ == '__main__':
    stage_args = osbuild.api.arguments()

    r = main(stage_args["tree"],
             stage_args["inputs"],
             stage_args["options"])
    sys.exit(r)
